## Repository to store presentations done by the 36th bootcamp


### 1st Group Project Repo

Leonardo Caldas  
Ruben Teixeira  
Mario Amaral  
https://github.com/RubenFCTeixeira/LeorumaPong  

Vitor Ferreira  
Manuel Sequeira  
Mario Ribeiro  
https://github.com/Dominor/briscolaofthree_pt  

Luis Henriques  
Diogo Saramago  
Antonio Machado  
https://github.com/DRS5/ScarysDayOff  

João Sena  
Ines Nunes  
Filipa Marta  
https://github.com/JHSena/AcademiaManager_Game  

Renato Coelho  
Sandra Pereira  
Daniel Pereira  
https://github.com/RenatoCoelhoPt/CatchTheShroom  

### 2nd Group Project Repo

Luis Henriques  
Mario Ribeiro  
Ruben Teixeira  
https://github.com/skarface-pt/PullTheRope  

Diogo Saramago  
Ines Nunes  
Vitor Ferreira  
https://github.com/DRS5/DIV_Programming_Quiz  

Mario Amaral  
João Sena  
Renato Coelho  
https://github.com/mario-amaral/Jorema  

Filipa Marta  
Manuel Sequeira  
Sandra Pereira  
https://github.com/filipamarta/the-magic-number_terminal-java-game  

Missing:
Leo Daniel António


### Missing Presentations:

Leonardo
Manuel

